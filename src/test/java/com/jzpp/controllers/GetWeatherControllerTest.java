package com.jzpp.controllers;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GetWeatherControllerTest {
    private GetWeatherController getWeatherController = new GetWeatherController();

    @Test
    public void testWithStandardData() throws JSONException {
        //given

        //when
        Object request = getWeatherController.request();

        //then
        assertThat(request).isNotNull();
        JSONObject actual = new JSONObject(request.toString());
        assertThat(actual.getString("name")).isEqualTo("Krakow");
    }
}