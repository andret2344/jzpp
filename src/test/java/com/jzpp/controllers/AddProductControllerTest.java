package com.jzpp.controllers;


import com.jzpp.model.Product;
import com.jzpp.model.ProductRepository;
import com.jzpp.model.Tag;
import com.jzpp.model.TagRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddProductControllerTest {
    private final String TAG_NAME = "TEST TAG";
    private final Tag tag = Tag.builder().name(TAG_NAME).build();
    @Mock
    private TagRepository tagRepository;
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private AddProductController addProductController;

    @Test
    public void addStandardProduct() {
        //given
        Optional<Long> id = Optional.empty();
        String name = "TEST NAME";
        String description = "TEST DESCRIPTION";
        int amount = 1;
        double price = 10.0;
        List<String> tags = Collections.singletonList(TAG_NAME);
        when(tagRepository.findByName(anyString())).thenReturn(tag);
        ArgumentCaptor<Product> product = ArgumentCaptor.forClass(Product.class);

        //when
        boolean result = addProductController.request(id, name, description, amount, price, tags);
        
        //then
        assertThat(result).isTrue();
        verify(productRepository).save(product.capture());
        assertThat(product.getValue()).isEqualTo(Product.builder().amount(amount).description(description).name(name).price(price).tags(Collections.singletonList(tag)).build());
    }
}