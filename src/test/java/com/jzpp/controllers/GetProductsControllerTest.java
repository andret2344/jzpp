package com.jzpp.controllers;

import com.jzpp.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;
import twitter4j.TwitterException;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetProductsControllerTest {
    private final Tag tag = Tag.builder().name("TEST TAG").build();
    private final List<Tag> tags = Collections.singletonList(tag);
    private final Product product = Product.builder().tags(tags).build();

    @Mock
    private TagRepository tagRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private GetTweetsController getTweetsController;
    @Mock
    private HttpServletRequest request;
    @Mock
    private Model model;

    @InjectMocks
    private GetProductsController getProductsController;

    @Test
    public void getNormalProduct() throws TwitterException {
        //given
        when(productRepository.findAll()).thenReturn(Collections.singletonList(product));
        when(getTweetsController.request(any(), any(), anyString())).thenReturn(1L);
        //when
        List<ProductWrapper> result = getProductsController.requestAll(request, model);
        //then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getDiscount()).isEqualTo(1L);
        assertThat(result.get(0).getProduct()).isEqualTo(product);
    }
}