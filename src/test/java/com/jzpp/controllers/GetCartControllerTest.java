package com.jzpp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetCartControllerTest {
    private final long productId = 10;
    private final int productAmount = 2;
    private final String CART = "cart";
    private final String RESULT = "result";
    private Map<Long, Integer> map = new HashMap<>();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;

    @InjectMocks
    private GetCartController getCartController;

    @Test
    public void getNormalCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(CART)).thenReturn(map);
        map.put(productId, productAmount);
        //when
        Map<String, Object> result = getCartController.get(request);
        //then
        assertThat(result.get(RESULT)).isEqualTo(0);
        assertThat(result.get(CART)).isEqualTo(map);
    }

    @Test
    public void getEmptyCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(CART)).thenReturn(null);
        //when
        Map<String, Object> result = getCartController.get(request);
        //then
        assertThat(result.get(RESULT)).isEqualTo(9);
    }

}