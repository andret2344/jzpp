package com.jzpp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;
import twitter4j.TwitterException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetTweetsControllerTest {
    private final String TAG = "tag";

    @Mock
    private HttpServletRequest request;
    @Mock
    private Model model;
    @Mock
    private HttpSession session;


    @InjectMocks
    private GetTweetsController getTweetsController;

    @Test
    public void getTweetsNoUser() throws TwitterException {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("username")).thenReturn(null);
        //when
        long result = getTweetsController.request(request, model, TAG);
        //then
        assertThat(result).isEqualTo(0);
    }
}