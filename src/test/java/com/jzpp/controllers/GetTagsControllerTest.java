package com.jzpp.controllers;

import com.jzpp.model.Product;
import com.jzpp.model.ProductRepository;
import com.jzpp.model.Tag;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetTagsControllerTest {
    private final Tag tag = Tag.builder().name("TEST TAG").build();
    private final List<Tag> tags = Collections.singletonList(tag);
    private final Product product = Product.builder().tags(tags).build();
    private final Optional<Product> optionalProduct = Optional.of(product);
    private final String RESULT = "result";
    private final String RESPONSE = "response";

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private GetTagsController getTagsController;


    @Test
    public void testWithStandardData() {
        //given
        when(productRepository.findById(any())).thenReturn(optionalProduct);
        //when
        Map<String, Object> request = getTagsController.request(product.getId());
        //then
        assertThat(request.get(RESPONSE)).isEqualTo(0);
        assertThat(request.get(RESULT)).isEqualTo(tags);
    }

    @Test
    public void testWithWrongId() {
        //given
        when(productRepository.findById(any())).thenReturn(Optional.empty());
        //when
        Map<String, Object> request = getTagsController.request(product.getId());
        //then
        assertThat(request.get(RESPONSE)).isEqualTo(1);
    }
}