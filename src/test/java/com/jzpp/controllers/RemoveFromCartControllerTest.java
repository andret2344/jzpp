package com.jzpp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class RemoveFromCartControllerTest {
    private final String CART = "cart";
    private final String USERNAME = "username";
    private final long firstProductId = 10;
    private final int firstProductAmount = 2;
    private final long secondProductId = 13;
    private final int secondProductAmount = 8;

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;

    @InjectMocks
    private RemoveFromCartController removeFromCartController;

    @Test
    public void shouldRemoveAllFromExistingCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USERNAME)).thenReturn(USERNAME);
        when(session.getAttribute(CART)).thenReturn(mock(Map.class));
        //when
        boolean result = removeFromCartController.removeAll(request);
        //then
        verify(session).setAttribute(CART, null);
        assertThat(result).isTrue();
    }

    @Test
    public void shouldRemoveOneFromExistingCart() {
        //given
        Map<Long, Integer> cart = new HashMap<>();
        cart.put(firstProductId, firstProductAmount);
        cart.put(secondProductId, secondProductAmount);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USERNAME)).thenReturn(USERNAME);
        when(session.getAttribute(CART)).thenReturn(cart);
        Map<Long, Integer> targetCart = new HashMap<>();
        targetCart.put(secondProductId, secondProductAmount);
        //when
        boolean result = removeFromCartController.removeOne(request, firstProductId);
        //then
        assertThat(result).isTrue();
        assertThat(cart.size()).isEqualTo(1);
        assertThat(cart.containsKey(secondProductId)).isTrue();
        verify(session).setAttribute(CART, targetCart);
    }

    @Test
    public void shouldFailNoCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USERNAME)).thenReturn(USERNAME);
        //when
        boolean result = removeFromCartController.removeOne(request, firstProductId);
        //then
        verify(session, never()).setAttribute(anyString(), any());
        assertThat(result).isFalse();
    }

    @Test
    public void shouldFailNotSignedIn() {
        //given
        when(request.getSession()).thenReturn(session);
        //when
        boolean result = removeFromCartController.removeOne(request, firstProductId);
        //then
        verify(session, never()).setAttribute(anyString(), any());
        assertThat(result).isFalse();
    }
}