package com.jzpp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChangeCartControllerTest {
    private final long productId = 10;
    private final int oldProductAmount = 2;
    private final int newProductAmount = 87374;
    private final int zeroProductAmount = 0;
    private final int negativeProductAmount = -3456789;
    private final String CART = "cart";
    private final String USERNAME = "username";
    @Mock
    private RemoveFromCartController removeFromCartController;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;

    @InjectMocks
    private ChangeCartController changeCartController;

    @Test
    public void shouldChangeInCart() {
        //given
        Map<Long, Integer> map = new HashMap<>();
        map.put(productId, oldProductAmount);
        Map<Long, Integer> targetMap = new HashMap<>();
        targetMap.put(productId, newProductAmount);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USERNAME)).thenReturn(USERNAME);
        when(session.getAttribute(CART)).thenReturn(map);
        //when
        boolean result = changeCartController.change(request, productId, newProductAmount);
        //then
        verify(session).setAttribute(CART, targetMap);
        assertThat(result).isTrue();
    }

    @Test
    public void shouldRemoveFromCart() {
        //given
        
        //when
        boolean result = changeCartController.change(request, productId, zeroProductAmount);
        //then
        verify(removeFromCartController).removeOne(request, productId);
        assertThat(result).isTrue();
    }

    @Test
    public void shouldFailNegativeAmount() {
        //given

        //when
        boolean result = changeCartController.change(request, productId, negativeProductAmount);
        //then
        assertThat(result).isFalse();
    }
}