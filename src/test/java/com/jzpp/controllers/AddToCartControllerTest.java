package com.jzpp.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddToCartControllerTest {
    private final long productId = 10;
    private final int productAmount = 2;
    private final int originalAmount = 4;
    private final String CART = "cart";
    private final String USERNAME = "username";
    private Map<Long, Integer> map = new HashMap<>();
    private Map<Long, Integer> targetMap = new HashMap<>();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;


    @InjectMocks
    private AddToCartController addToCartController;

    @Test
    public void shouldAddToSessionWithNonEmptyCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USERNAME)).thenReturn(USERNAME);
        when(session.getAttribute(CART)).thenReturn(map);
        map.put(productId, originalAmount);
        targetMap.put(productId, productAmount + originalAmount);
        //when
        boolean result = addToCartController.add(request, productId, productAmount);
        //then
        verify(session).setAttribute(CART, targetMap);
        assertThat(result).isTrue();
    }


    @Test
    public void shouldAddToSessionWithEmptyCart() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("username")).thenReturn(USERNAME);
        when(session.getAttribute("cart")).thenReturn(map);
        map.clear();
        targetMap.put(productId, productAmount);
        //when
        boolean result = addToCartController.add(request, productId, productAmount);
        //then
        verify(session).setAttribute(CART, targetMap);
        assertThat(result).isTrue();
    }

    @Test
    public void shouldFailNoSession() {
        //given

        //when
        boolean result = addToCartController.add(request, productId, productAmount);
        //then
        verify(session, never()).setAttribute(anyString(), any());
        assertThat(result).isFalse();
    }

    @Test
    public void shouldFailEmptySession() {
        //given
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("username")).thenReturn(null);
        //when
        boolean result = addToCartController.add(request, productId, productAmount);
        //then
        verify(session, never()).setAttribute(anyString(), any());
        assertThat(result).isFalse();
    }

}