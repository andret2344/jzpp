package com.jzpp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@Controller
public class ChangeCartController {
    private final Logger LOGGER = LogManager.getLogger(ChangeCartController.class);
    private final RemoveFromCartController removeFromCartController;

    @RequestMapping(value = "/api/cart/change", method = RequestMethod.POST)
    @ResponseBody
    public boolean change(HttpServletRequest request,
                          @RequestParam("productId") long productId,
                          @RequestParam("productAmount") int productAmount
    ) {
        LOGGER.info("Change of product {} in cart", productId);
        LOGGER.debug("New amount {}", productAmount);
        if (productAmount == 0) {
            removeFromCartController.removeOne(request, productId);
            return true;
        }
        if (productAmount < 0) {
            return false;
        }
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            return false;
        }
        Map<Long, Integer> cart = (Map<Long, Integer>) request.getSession().getAttribute("cart");
        if (cart == null || !cart.containsKey(productId)) {
            return false;
        }
        cart.put(productId, productAmount);
        request.getSession().setAttribute("cart", cart);
        return true;
    }
}
