package com.jzpp.controllers;

import com.jzpp.model.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import twitter4j.TwitterException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@AllArgsConstructor
@Slf4j
@Controller
public class GetProductsController {
    private final TagRepository tagRepository;
    private final ProductRepository productRepository;
    private final GetTweetsController getTweetsController;

    private final Logger LOGGER = LogManager.getLogger(GetProductsController.class);

    @PostConstruct
    public void init() {
        if (productRepository.findAll().isEmpty()) {
            LOGGER.warn("Product repository was empty, filling");
            Tag wood = Tag.builder().name("wood").build();
            Tag table = Tag.builder().name("table").build();
            Tag chair = Tag.builder().name("chair").build();
            tagRepository.save(wood);
            tagRepository.save(table);
            tagRepository.save(chair);
            productRepository.save(Product.builder().name("chair").price(149.99).amount(2).description("comfy chair").tags(Arrays.asList(wood, chair)).build());
            productRepository.save(Product.builder().name("table").price(249.99).amount(2).description("wooden table").tags(Arrays.asList(wood, table)).build());
        }
    }

    @RequestMapping(value = "/api/products/get", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductWrapper> requestAll(HttpServletRequest request,
                                           Model model) {
        LOGGER.info("All products are being returned");
        List<Product> products = productRepository.findAll();
        Map<Tag, Long> applicableTags = products.stream()
                .flatMap(product -> product.getTags().stream())
                .distinct()
                .collect(Collectors.toMap(tag -> tag, tag -> {
                    try {
                        return getTweetsController.request(request, model, tag.getName());
                    } catch (TwitterException e) {
                        LOGGER.error("Catastrophic failure of Twitter");
                        return null;
                    }
                }));
        List<ProductWrapper> result = new ArrayList<>();
        for (Product product : products) {
            int discount = 0;
            for (Tag tag : product.getTags()) {
                discount += applicableTags.get(tag);
            }
            discount = discount > 20 || discount < 0 ? 20 : discount;
            result.add(new ProductWrapper(discount, product));
        }
        return result;
    }
}
