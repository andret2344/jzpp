package com.jzpp.controllers;

import com.jzpp.misc.TwitterUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import twitter4j.TwitterException;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@Slf4j
@Controller
public class GetTweetsController {
    private final TwitterUtils twitterUtils;
    private final Logger LOGGER = LogManager.getLogger(GetTweetsController.class);

    @RequestMapping(value = "/api/tweets/get/{tag}", method = RequestMethod.GET)
    @ResponseBody
    public long request(HttpServletRequest request,
                        Model model,
                        @PathVariable("tag") String tag
    ) throws TwitterException {
        LOGGER.info("Getting tweets by tag");
        Object username = request.getSession().getAttribute("username");
        if (username == null) {
            LOGGER.error("No user");
            return 0;
        }
        return twitterUtils.searchTweetsByUser((String) username).stream()
                .filter(status -> status.getText().contains(tag))
                .count();
    }
}
