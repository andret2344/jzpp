package com.jzpp.controllers;

import com.jzpp.model.Product;
import com.jzpp.model.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@AllArgsConstructor
@Slf4j
@Controller
public class GetTagsController {
    private final ProductRepository productRepository;

    private final Logger LOGGER = LogManager.getLogger(GetProductsController.class);

    @RequestMapping(value = "/api/tags/get/{productId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> request(
            @PathVariable("productId") long productId
    ) {
        LOGGER.info("Get tags for product of id {}", productId);
        Map<String, Object> result = new HashMap<>();
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            result.put("response", 0);
            result.put("result", optionalProduct.get().getTags());
        } else {
            LOGGER.error("Product of id {} was not found", productId);
            result.put("response", 1);
        }
        return result;
    }


}