package com.jzpp.controllers;

import com.jzpp.model.Product;
import com.jzpp.model.ProductRepository;
import com.jzpp.model.Tag;
import com.jzpp.model.TagRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@Controller
public class AddProductController {
    private final Logger LOGGER = LogManager.getLogger(AddProductController.class);

    private final ProductRepository productRepository;
    private final TagRepository tagRepository;

    @RequestMapping(value = "/api/products/add", method = RequestMethod.POST)
    @ResponseBody
    public boolean request(
            @RequestParam("id") Optional<Long> id,
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @RequestParam("amount") int amount,
            @RequestParam("price") double price,
            @RequestParam("tags[]") List<String> tags
    ) {
        LOGGER.info("Tags are being added");
        for (String tag : tags) {
            if (tagRepository.findByName(tag) == null) {
                tagRepository.save(Tag.builder().name(tag).build());
            }
        }
        if (id.isPresent()) {
            LOGGER.info("Product is being updated");
            productRepository.save(Product.builder()
                    .id(id.get())
                    .name(name)
                    .description(description)
                    .amount(amount)
                    .price(price)
                    .tags(tags.stream().map(tagRepository::findByName).collect(Collectors.toList()))
                    .build()
            );
            return true;
        } else {
            LOGGER.info("Product is being added");
            productRepository.save(Product.builder()
                    .name(name)
                    .description(description)
                    .amount(amount)
                    .price(price)
                    .tags(tags.stream().map(tagRepository::findByName).collect(Collectors.toList()))
                    .build()
            );
            return true;
        }
    }
}
