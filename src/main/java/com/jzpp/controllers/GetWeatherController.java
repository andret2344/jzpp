package com.jzpp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@AllArgsConstructor
@Slf4j
@Controller
public class GetWeatherController {
    private final Logger LOGGER = LogManager.getLogger(GetWeatherController.class);

    @RequestMapping(value = "/api/weather/get", method = RequestMethod.POST)
    @ResponseBody
    public Object request() {
        String SERVER_URL =
                "http://api.openweathermap.org/data/2.5/weather?q=Krakow&appid=855dabbfca6e31d4378f474b1c8cc5a5";
        try {
            LOGGER.info("Getting weather");
            URL url = new URL(SERVER_URL);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();

            InputStream isResponse = urlConnection.getInputStream();
            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(isResponse));

            String myLine;
            StringBuilder stringBuilder = new StringBuilder();
            while ((myLine = responseBuffer.readLine()) != null) {
                stringBuilder.append(myLine);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            LOGGER.error("Something happened {}", e.getMessage());
            return null;
        }
    }
}