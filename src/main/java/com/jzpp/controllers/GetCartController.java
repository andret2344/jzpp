package com.jzpp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@Controller
public class GetCartController {
    private final Logger LOGGER = LogManager.getLogger(GetCartController.class);

    @RequestMapping(value = "/api/cart/get", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> get(HttpServletRequest request) {
        LOGGER.info("Cart get called");
        Map<String, Object> result = new HashMap<>();
        Object cart = request.getSession().getAttribute("cart");
        if (cart == null) {
            LOGGER.warn("Cart empty");
            result.put("result", 9);
        } else {
            result.put("result", 0);
            result.put("cart", cart);
        }
        return result;
    }
}
