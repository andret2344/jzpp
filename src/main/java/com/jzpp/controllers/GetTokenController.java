package com.jzpp.controllers;

import com.jzpp.misc.TwitterUtils;
import com.jzpp.model.User;
import com.jzpp.model.UserRepository;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@Slf4j
@Controller
public class GetTokenController {
    private final UserRepository userRepository;
    private final Logger LOGGER = LogManager.getLogger(GetProductsController.class);
    private final TwitterUtils twitterUtils;

    @Autowired
    public GetTokenController(UserRepository userRepository, TwitterUtils twitterUtils) {
        this.userRepository = userRepository;
        this.twitterUtils = twitterUtils;
    }

    @RequestMapping({"", "/", "/index", "*"})
    public String index(HttpServletRequest request) {
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            return "indexLogout";
        }
        request.getSession().setAttribute("admin", userRepository.findById((long) request.getSession().getAttribute("id")).isAdmin());
        return "indexLogin";
    }

    @RequestMapping("/getToken")
    public RedirectView request(HttpServletRequest request) throws TwitterException {
        Twitter twitter = twitterUtils.getTwitterInstanceForUser();
        String callbackUrl = "http://localhost:8080/twitterCallback";

        RequestToken requestToken = twitter.getOAuthRequestToken(callbackUrl);
        request.getSession().setAttribute("requestToken", requestToken);

        request.getSession().setAttribute("twitter", twitter);
        String twitterUrl = requestToken.getAuthorizationURL();
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(twitterUrl);
        return redirectView;
    }

    @RequestMapping("/twitterCallback")
    public String twitterCallback(@RequestParam(value = "oauth_verifier", required = false) String oauthVerifier,
                                  @RequestParam(value = "denied", required = false) String denied,
                                  HttpServletRequest request, HttpServletResponse response, Model model) throws TwitterException {

        LOGGER.debug("Twitter callback");
        if (denied != null) {
            return "redirect:/";
        }
        Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
        RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
        AccessToken token = twitter.getOAuthAccessToken(requestToken, oauthVerifier);
        model.addAttribute("username", twitter.getScreenName());
        request.getSession().setAttribute("username", twitter.getScreenName());
        request.getSession().setAttribute("id", twitter.getId());
        if (userRepository.findById(twitter.getId()) == null) {
            userRepository.save(User.builder().admin(false).userId(twitter.getId()).build());
        }
        return "redirect:/";
    }

    @RequestMapping("/logout")
    public String shutdown(HttpServletRequest request) {
        LOGGER.info("Logging out");
        request.getSession().invalidate();
        return "redirect:/";
    }

    @RequestMapping("/twitterLoggedIn")
    public String afterLogin(HttpServletRequest request) {
        return "redirect:/";
    }
}
