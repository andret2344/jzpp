package com.jzpp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@Controller
public class RemoveFromCartController {
    private final Logger LOGGER = LogManager.getLogger(RemoveFromCartController.class);

    @RequestMapping(value = "/api/cart/remove/all", method = RequestMethod.POST)
    @ResponseBody
    public boolean removeAll(HttpServletRequest request) {
        LOGGER.info("Removing all products from cart");
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            return false;
        }
        Map<Long, Integer> cart = (Map<Long, Integer>) request.getSession().getAttribute("cart");
        if (cart == null) {
            LOGGER.warn("Empty cart during removing");
            return false;
        }
        request.getSession().setAttribute("cart", null);
        return true;
    }

    @RequestMapping(value = "/api/cart/remove/one", method = RequestMethod.POST)
    @ResponseBody
    public boolean removeOne(HttpServletRequest request,
                             @RequestParam("productId") long productId
    ) {
        LOGGER.info("Removing product of id {} from cart", productId);
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            LOGGER.warn("No user");
            return false;
        }
        Map<Long, Integer> cart = (Map<Long, Integer>) request.getSession().getAttribute("cart");
        if (cart == null || !cart.containsKey(productId)) {
            return false;
        } else {
            cart.remove(productId);
        }
        request.getSession().setAttribute("cart", cart);
        return true;
    }
}
