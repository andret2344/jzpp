package com.jzpp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@Controller
public class AddToCartController {
    private final Logger LOGGER = LogManager.getLogger(AddToCartController.class);

    @RequestMapping(value = "/api/cart/add", method = RequestMethod.POST)
    @ResponseBody
    public boolean add(HttpServletRequest request,
                       @RequestParam("productId") long productId,
                       @RequestParam("productAmount") int productAmount
    ) {
        LOGGER.info("Product of id {} added to cart in amount of {}", productId, productAmount);
        if (request.getSession() == null || request.getSession().getAttribute("username") == null) {
            LOGGER.warn("User not authenticated");
            return false;
        }
        Map<Long, Integer> cart = (Map<Long, Integer>) request.getSession().getAttribute("cart");
        if (cart == null) {
            cart = new HashMap<>();
        }
        if (cart.containsKey(productId)) {
            cart.put(productId, cart.get(productId) + productAmount);
        } else {
            cart.put(productId, productAmount);
        }
        request.getSession().setAttribute("cart", cart);
        return true;
    }
}
