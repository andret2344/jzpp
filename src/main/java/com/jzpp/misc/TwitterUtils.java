package com.jzpp.misc;

import org.springframework.stereotype.Component;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.List;

@Component
public final class TwitterUtils {
    public List<Status> searchTweetsByUser(String user) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        return twitter.getUserTimeline(user, new Paging(1, 300));
    }

    public List<Status> searchTweetsByTag(String tag) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Query query = new Query(tag);
        QueryResult result = twitter.search(query);
        return result.getTweets();
    }

    public Twitter getTwitterInstance() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("RjwBzWE3UMVfniI9ei7LwSpMl")
                .setOAuthConsumerSecret("qfEcPEMDU8KkXoFgT0THx06ITXmqpE3qfICyJZTjKa9WNuKYdq")
                .setOAuthAccessToken("262093905-8DWja71qd9AcgsZU1cxoTiPDpkX28DDLR1i8vpeI")
                .setOAuthAccessTokenSecret("uGqN020Jpoec2OXT88Mzw2WAmtCsSF42R3ZchL3iKAluO");
        TwitterFactory tf = new TwitterFactory(cb.build());
        return tf.getInstance();
    }


    public Twitter getTwitterInstanceForUser() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("RjwBzWE3UMVfniI9ei7LwSpMl")
                .setOAuthConsumerSecret("qfEcPEMDU8KkXoFgT0THx06ITXmqpE3qfICyJZTjKa9WNuKYdq");
        TwitterFactory tf = new TwitterFactory(cb.build());
        return tf.getInstance();
    }
}
