package com.jzpp.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"id"})
public class Product {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private double price;
    private int amount;
    private String description;
    @ManyToMany
    @JoinTable(name = "products_tags",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id")
    )
    private List<Tag> tags;
}
