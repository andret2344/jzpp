package com.jzpp.model;

import lombok.Value;

@Value
public class ProductWrapper {
    private int discount;
    private Product product;
}