package com.jzpp.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"id"})
public class Tag {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue
    private long id;
    private String name;
}
