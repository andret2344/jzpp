package com.jzpp.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TagRepository extends CrudRepository<Tag, Integer> {
    @Override
    List<Tag> findAll();

    Tag findByName(String name);
}
