package com.jzpp.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class User {
    @Id
    private long userId;
    private boolean admin;
}
