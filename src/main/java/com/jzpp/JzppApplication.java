package com.jzpp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JzppApplication {

    public static void main(String[] args) {
        SpringApplication.run(JzppApplication.class, args);
    }
}
