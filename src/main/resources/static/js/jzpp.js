Vue.use(VueResource);
const jzpp = new Vue({
    el: "#jzpp",
    data: {
        products: [],
        cart: [],
        editing: null,
        weather: ""
    },
    methods: {
        updateProducts: function () {
            this.$http.get("/api/products/get").then(
                response => {
                    this.products = [];
                    response.body.forEach(element => {
                        let prod = element.product;
                        this.products.push({
                            discount: element.discount,
                            product: new Product(prod.id, prod.name, prod.amount, prod.price, prod.description, prod.tags)
                        });
                    });
                },
                response => console.log(response)
            );
        },
        updateCart: function () {
            this.$http.post("/api/cart/get").then(
                response => {
                    if (response.data.result === 0) {
                        this.cart = [];
                        for (let item in response.data.cart) {
                            let product = this.products.find(p => p.product.getId() === parseInt(item));
                            this.cart.push({item: product, amount: response.data.cart[item]});
                        }
                    }
                },
                response => console.log(response)
            );
        },
        addProduct: function () {
            this.$http.post("/api/products/add", {
                name: $("#new-name").val(),
                description: $("#new-description").val(),
                amount: parseInt($("#new-amount").val()),
                price: parseFloat($("#new-price").val()),
                tags: $("#new-tags").val().split(/, ?/)
            }, {emulateJSON: true}).then(
                () => {
                    this.updateProducts();
                    $("#new-amount, #new-description, #new-name, #new-price, #new-tags").val("");
                }
            );
        },
        edit: function (product) {
            this.editing = product;
        },
        done: function () {
            this.$http.post("/api/products/add", {
                id: parseInt($("#edit-id").text()),
                name: $("#edit-name").val(),
                description: $("#edit-description").val(),
                amount: parseInt($("#edit-amount").val()),
                price: parseFloat($("#edit-price").val()),
                tags: $("#edit-tags").val().split(/, ?/)
            }, {emulateJSON: true}).then(
                () => {
                    this.editing = null;
                    this.updateProducts();
                    $("#new-amount, #new-description, #new-name, #new-price, #new-tags").val("");
                }
            );
        },
        add: function (product) {
            this.$http.post("/api/cart/add", {productId: product.getId(), productAmount: 1}, {emulateJSON: true}).then(
                response => {
                    if (response.data) {
                        this.updateCart();
                    }
                }
            )
        },
        getWeather: function () {
            this.$http.post("/api/weather/get").then(
                response => {
                    console.log(response.data);
                    this.weather = response.data.weather[0].description;
                }
            )
        }
    },
    mounted: function () {
        this.updateProducts();
        this.updateCart();
        this.getWeather();
        setInterval(this.getWeather, 120 * 1000);
    }
});
