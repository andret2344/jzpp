class Product {
    constructor(id, name, amount, price, description, tags) {
        this._id = id;
        this._name = name;
        this._amount = amount;
        this._price = price;
        this._description = description;
        this._tags = tags;

        this.getId = function() {
            return this._id;
        };

        this.getName = function() {
            return this._name;
        };

        this.getAmount = function() {
            return this._amount;
        };

        this.getPrice = function() {
            return this._price;
        };

        this.getDescription = function() {
            return this._description;
        };

        this.getTags = function() {
            return this._tags;
        };

        this.getTagsAsString = function() {
            let result = "";
            for (let tag of this._tags) {
                result += ", " + tag.name;
            }
            return result.substr(2);
        }
    }
}
