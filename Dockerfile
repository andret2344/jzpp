FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
CMD java -Dspring.profiles.active=prod -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar app.jar
